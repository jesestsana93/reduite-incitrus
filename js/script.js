$(document).ready(function(){
	menu();
	menuMobile();
	headerMobile();
	$('#nav-icon2').click(function(){
		$(this).toggleClass('open');
		$('#menu-mobile').toggleClass('open');
	});

	$('.clickMenu').click(function(){
		if ($('.optsMenu').not($(this).find($('.optsMenu'))).hasClass('open')) {
			$('.optsMenu').not($(this).find($('.optsMenu'))).removeClass('open');
		}
		if ($(this).find(".optsMenu").hasClass('open')) {
			$(this).find(".optsMenu").removeClass('open');
		}else{
			$(this).find(".optsMenu").addClass('open');
		}
	});
});
var pageName = window.location.pathname.split('/')[window.location.pathname.split('/').length-1].substring(0,4)
var pathImg = '';
var path = '';
function menu(){
	console.log(pageName)
	if (pageName == 'bole') {
		pathImg = '../image';
		path = '../';
	}else{
		pathImg = 'image';
	}
	$('header').append(`
		<nav id="menu" class="navbar navbar-toggleable-md navbar-inverse nav-stacked">
			 <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       			<span class="navbar-toggler-icon"></span>
      		</button>
      		<a href="index.html">
    			<img src="image/svg/reduite-logotipo.svg" width="250" class="d-inline-block align-top" alt="">
  			</a>
			<ul>				
				<li><a data-scroll href="#inicio" class="active" ><span class="icon-house"></span>inicio</a></li>
				<li><a data-scroll href="#reduite" ><span class="icon-suitcase"></span>¿qué es reduite?</a></li>
				<li><a data-scroll href="#contacto"><span class="icon-mail"></span>contacto</a></li>
				<li><a href="#"><span class="icon-telefono"></span><img src="image/svg/telefono-menu.svg" alt="telefono-menu" class="telefono"></a></li>
				<li><a href="https://www.facebook.com/reduite.mx" target="blank"><span class="icon-facebook"></span><img src="image/svg/facebook.svg" alt="facebook" width="30" height="30"></a></li>
				<li><a href="#" target="blank"><span class="icon-instagram"></span><img src="image/svg/instagram.svg" alt="instagram" width="30" height="30" ></a></li>
			</ul>
		</nav>
	`)
};

function menuMobile(){
	if (pageName == 'bole') {
		pathImg = '../image';
		path = '../';
	}else{
		pathImg = 'image';
	}
	$('#menu-mobile').append(`
		<div class="cont-menu">			
			<div class="clickMenu">
				<div class="op">
					<a href="#inicio" >inicio</a>
				</div>
			</div>
			<div class="clickMenu">
				<div class="op">
					<a href="#reduite">¿qué es reduite?</a>
				</div>
			</div>
			<div class="clickMenu">
				<div class="op">
					<a href="#contacto">contacto</a>
				</div>
			</div>
			<div class="clickMenu">
				<div class="op1">
					<a href="#contacto"><img src="image/svg/telefono-menu.svg" alt="telefono" id="telefono"></a> <br>

					<a href="https://www.facebook.com/reduite.mx" target="blank"><img src="image/svg/facebook.svg" alt="facebook" class="redes"></a>
					<a href="#"><img src="image/svg/instagram.svg" alt="instagram" class="redes"></a>
				</div>
			</div>			
		</div>
	`);
};

function headerMobile(){
	if (pageName == 'bole') {
		pathImg = '../image';
		path = '../';
	}else{
		pathImg = 'image';
	}
	$('#header-mobile').append(`
		<a href="index.html">
			<img src="`+pathImg+`/svg/reduite-logotipo.svg" alt="logoReduite" class="image-responsive2">
		</a>
		<div id="nav-icon2">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	`);
}